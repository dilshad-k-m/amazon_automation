package practice;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;

public class Amazon_automation {
    public static void signup(WebDriver driver) throws InterruptedException {
        System.setProperty("web-driver.chrome.driver", "/usr/bin/chromedriver");
        driver.get("http://www.amazon.com");
        WebElement signup = driver.findElement(By.linkText("Sign in"));
        signup.click();
        WebElement reg = driver.findElement(By.linkText("Create your Amazon account"));
        reg.click();
        WebElement name = driver.findElement(By.id("ap_customer_name"));
        name.sendKeys("dilshad km");
        Thread.sleep(2000);
        WebElement mobile = driver.findElement(By.name("email"));
        mobile.sendKeys("+91 8330816990");
        Thread.sleep(2000);
        WebElement pswd = driver.findElement(By.xpath("//input[@type='password']"));
        pswd.sendKeys("dilshad");
        Thread.sleep(2000);
        WebElement re = driver.findElement(By.cssSelector("input[name='passwordCheck']"));
        re.sendKeys("dilshad");
        Thread.sleep(2000);
        WebElement verify = driver.findElement(By.id("continue"));
        verify.click();
        Thread.sleep(2000);
        String url = "https://www.amazon.com/ap/cvf/request";
        String curl = driver.getCurrentUrl();
        if (curl.contains(url)) {
            System.out.println("Registration success");
        } else {
            System.out.println("Invalid data");
        }

    }
    public static void search(WebDriver driver) throws InterruptedException{
        System.setProperty("web-driver.chrome.driver", "chromedriver");
        driver.get("https://www.amazon.com");
        WebElement search = driver.findElement(By.cssSelector("input[placeholder='Search Amazon']"));
        search.sendKeys("iphone");
        Actions actions=new Actions(driver);
        WebElement searchbar= driver.findElement(By.cssSelector("input[type='submit']"));
        searchbar.click();
        WebElement first=driver.findElement(By.className("s-image"));
        first.click();
        driver.navigate().back();
        Thread.sleep(2000);
        String eurl="https://www.amazon.com/s?k=iphone";
        String aurl=driver.getCurrentUrl();
        if(aurl.contains(eurl)){
            System.out.println("Search success");
        }
        else{
            System.out.println("Search failed");
        }
            Thread.sleep(2000);
    }
    public static void scroll(WebDriver driver) throws InterruptedException{
        driver.get("https://www.amazon.com");
        WebElement x=driver.findElement(By.id("a-popover-root"));
        Actions actions=new Actions(driver);
        actions.moveToElement(x);
        actions.perform();
        Thread.sleep(3000);
        WebElement y=driver.findElement(By.cssSelector("input[placeholder='Search Amazon']"));
        actions.moveToElement(y).perform();
        Thread.sleep(2000);
        actions.moveToElement(x).build().perform();
        actions.moveToElement(y).perform();
        actions.doubleClick(y).perform();
        actions.moveToElement(y).click().keyDown(y, Keys.SHIFT).sendKeys(y,"iphone").keyUp(y, Keys.SHIFT).doubleClick(y).build().perform();
        Thread.sleep(2000);
        actions.doubleClick(y).perform();
        Thread.sleep(2000);
    }
    public static void addtocart(WebDriver driver) throws InterruptedException{
        driver.get("https://amazon.com");
        WebElement login= driver.findElement(By.linkText("Sign in"));
        login.click();
        WebElement mobile=driver.findElement(By.id("ap_email"));
        mobile.sendKeys("8330816990");
        WebElement confirm =driver.findElement(By.id("continue"));
        confirm.click();
        WebElement pswd=driver.findElement(By.name("password"));
        pswd.sendKeys("dilshad@20");
        WebElement submit=driver.findElement(By.id("signInSubmit"));
        submit.click();
        WebElement search=driver.findElement(By.id("twotabsearchtextbox"));
        search.sendKeys("smartwatch");
        WebElement search_button=driver.findElement(By.xpath("//input[@value='Go']"));
        search_button.click();
        Thread.sleep(2000);
        WebElement firstSearchResult = driver.findElement(By.xpath("//div[@class='a-section aok-relative s-image-fixed-height']"));
        firstSearchResult.click();
        Thread.sleep(2000);
        WebElement cartbutton=driver.findElement(By.name("submit.addToCart"));
        cartbutton.click();
        Thread.sleep(2000);
        String etitle="Amazon.com Shopping Cart";
        String atitle=driver.getTitle();
        if(atitle.equals(etitle)){
            System.out.println("Item added to cart successfully");
        }
        else{
            System.out.println("Item not added");
        }
        Thread.sleep(3000);
        driver.quit();
    }
    public static void main(String[] args) throws InterruptedException{
        System.setProperty("web-driver.chrome.driver", "chromedriver");
        WebDriver driver = new ChromeDriver();
        signup(driver);
        search(driver);
        scroll(driver);
        addtocart(driver);
    }
}